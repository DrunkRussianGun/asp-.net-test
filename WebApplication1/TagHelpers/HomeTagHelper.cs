﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace WebApplication1.TagHelpers
{
	public class HomeTagHelper : TagHelper
	{
		public override void Process(TagHelperContext context, TagHelperOutput output)
		{
			output.TagName = "button";
			output.Content.SetContent("HomeTagHelper");
		}
	}
}
