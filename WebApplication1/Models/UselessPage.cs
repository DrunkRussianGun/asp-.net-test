﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Razor;

namespace WebApplication1.Models
{
	public class UselessPage : RazorPage<ErrorViewModel>
	{
		public string UselessText { get; set; }

		public override Task ExecuteAsync()
		{
			return Task.CompletedTask;
		}
	}
}
